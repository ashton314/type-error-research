#lang racket

(module+ test (require rackunit))

;;; This is a sample of a type checker for a little language. The language looks
;;; like this:
;;;
;;;     42                       ; (integers)
;;;     #t, #f                   ; (booleans)
;;;     (let (x 1) (+ x 1))      ; ⇒ 2
;;;     ((λ y (+ y 2)) 40)       ; ⇒ 42
;;;     (λ z (if (zero? z) 1 2)) ; ⇒ ℕ → 1 | 2
;;;     (zero? (+ 1 2))          ; ⇒ #f
;;;
;;; We get single-variable `let's and `lambda's, for simplicity's sake.
;;;
;;; Resources for understanding typechecking:
;;;  - https://cs.brown.edu/courses/cs173/2012/book/types.html#%28part._.Type_.Inference%29

;;; Phase 1: Constraint Generation
;;;
;;; Walk through the program and generate a *set* of constraints.
;;;
;;; AST nodes are respresented by this ast struct: pair an ast-node with
;;; metadata about where it appears in source.

(struct ast (node meta) #:transparent)

(struct constraint (reason) #:transparent)
(struct eqConstraint constraint (lhs rhs) #:transparent)

(struct tExpr (expr) #:transparent)
(struct tVar (name) #:transparent)
(struct tNum () #:transparent)
(struct tBool () #:transparent)
(struct tFunc (dom rng) #:transparent)
(struct tFuncPoly (dom rng) #:transparent)

(define (tFunc->Poly tf subst)
  (match tf
    [(tFunc d r)
     (tFuncPoly (walk d subst) (walk r subst))]))

(define (Poly->tFunc p)
  (tFunc (tFuncPoly-dom p) (tFuncPoly-rng p)))

;; constraint-gen :: expr → (set constraint)
(define (constraint-gen expr)
  (match expr
    ;; Literal values are just the type they are
    [(ast (? number?) _) (list (eqConstraint 'lit (tExpr expr) (tNum)))]
    [(ast (? boolean?) _) (list (eqConstraint 'lit (tExpr expr) (tBool)))]

    ;; Expressions that are symbols represent variables; the type of that
    ;; expression is whatever the type of the variable is. (Determined by the
    ;; context.)
    [(ast (? symbol? s) _) (list (eqConstraint 'var-ref (tExpr expr) (tVar s)))]

    ;; Some builtins: zero?
    [(ast `(zero? ,a) _)
     (append (constraint-gen a)
             ;; This is a choice we make with the first constraint: we don't
             ;; necessarily have to restrict the type of the argument to be a
             ;; number---we could consider `(zero? #t)' to be #f without a type
             ;; error. For purposes of this experiment, however, we're going to
             ;; restrict the argument type here to be just numbers.
             (list (eqConstraint 'op-arg (tExpr a) (tNum))
                   (eqConstraint 'op-res (tExpr expr) (tBool))))]

    ;; Math: generate the constraints for the left- and right-hand sides, then
    ;; add constraints that those whole expressions turn out to be numbers.
    [(ast (list (or '+ '- '* '/) lhs rhs) _)
     (append (constraint-gen lhs)
             (constraint-gen rhs)
             ;; The order in the equality constraints of expr → type
             ;; is important for the describe-error step to generate a
             ;; nice error message.
             (list (eqConstraint 'op-arg (tExpr lhs) (tNum))
                   (eqConstraint 'op-arg (tExpr rhs) (tNum))
                   (eqConstraint 'op-res (tExpr expr) (tNum))))]

    ;; Conditionals: two flavors, one that produces a type-union (TODO) and one
    ;; that enforces that both branches be of the same type.
    [(ast `(if ,c ,t-case ,f-case) _)
     (append (constraint-gen c)
             (constraint-gen t-case)
             (constraint-gen f-case)
             (list (eqConstraint 'cond-test (tExpr c) (tBool))
                   (eqConstraint 'cond-arm-match (tExpr t-case) (tExpr f-case))
                   (eqConstraint 'cond-expr (tExpr expr) (tExpr t-case))))]

    [(ast `(if-union ,c ,t-case ,f-case) _)
     (raise "type-union `if' clauses unimplemented")]

    ;; Let blocks
    [(ast `(let (,(? symbol? x) ,bind-val) ,body) _)
     (append (constraint-gen bind-val)
             (list (eqConstraint 'let-var (tVar x) (tExpr bind-val)))
             (constraint-gen body)
             (list (eqConstraint 'let-body (tExpr expr) (tExpr body))))]

    ;; Functions: gerneate constraints for the body, then synthesize an arrow
    ;; type.
    [(ast `(λ ,var ,body) _)
     (append (constraint-gen body)
             (list (eqConstraint 'fn-body (tExpr expr) (tFunc (tVar var) (tExpr body)))))]

    ;; Function calls: generate constraints for arguments and for the function,
    ;; then make sure the two line up.
    [(ast `(,f ,a) _)
     (append (constraint-gen f)
             (constraint-gen a)
             (list (eqConstraint 'fn-app (tExpr f) (tFunc (tExpr a) (tExpr expr)))))]))

;; A little utility to tag an AST (represented as a list as you would normally
;; write) into an AST written with the `ast' struct and tagged with random
;; symbols.
(define (tag-ast raw-ast)
  (match raw-ast
    [(? number? n) (ast n (gensym 'lit-num))]
    [(? boolean? b) (ast b (gensym 'lit-bool))]
    [(? symbol? s) (ast s (gensym 'var))]
    [`(,(and (or 'zero?) op) ,a) (ast `(,op ,(tag-ast a)) (gensym 'monop))]
    [`(,(and (or '+ '- '* '/) op) ,lhs ,rhs)
     (ast (list op (tag-ast lhs) (tag-ast rhs)) (gensym 'binop))]
    [`(let (,(? symbol? x) ,bind-val) ,body)
     (ast `(let (,x ,(tag-ast bind-val)) ,(tag-ast body)) (gensym 'let-block))]
    [`(if ,c ,tc ,fc)
     (ast `(if ,(tag-ast c) ,(tag-ast tc) ,(tag-ast fc)) (gensym 'if-block))]
    [`(λ ,var ,body) (ast `(λ ,var ,(tag-ast body)) (gensym 'fndef))]
    [(list f a) (ast (list (tag-ast f) (tag-ast a)) (gensym 'fncall))]))

;;; With that out of the way, let's test it.

(module+ test
  (test-case "constraint-gen"
    ;; Simple literals
    (check-equal? (constraint-gen (ast 42 'foo))
                  (list (eqConstraint 'lit (tExpr (ast 42 'foo)) (tNum))))
    (check-equal? (constraint-gen (ast #f 'foo))
                  (list (eqConstraint 'lit (tExpr (ast #f 'foo)) (tBool))))

    ;; Lot of constraints generated here. We won't test that they are all
    ;; individually in the set. Instead, we'll just check the cardinality of the
    ;; constraint set.
    (check-= (length (constraint-gen (tag-ast '(+ x 1))))
             5 0)

    (check-= (length (constraint-gen (tag-ast '((λ x (+ x 42)) 1))))
             8 0)

    (check-= (length (constraint-gen (tag-ast '(let (x 1) (+ x 1)))))
             8 0)

    (check-= (length (constraint-gen (tag-ast '(if (zero? x) 1 2))))
             8 0)))

;;; Unification: this is the heart of the type checker.
;;;
;;; We deviate here from the algorithm in PLAI: we instead implement unify
;;; inspired by μKanren: instead of modifying the substitution structure as PLAI
;;; and EPL do, we call `walk' on the terms we're trying to unify.
;;;
;;; This should both avoid having to mutate the substitution, and should also
;;; give us a structure that we can walk to try and find authoritative sources
;;; of type information during error reporting.

;;; subst :: var → term
;;; walk  :: term × subst → term
;;; ext-s :: var × term × subst → subst

(define (var? v)
  (or (tExpr? v) (tVar? v)))

(define (var=? u v)
  (and (var? u)
       (var? v)
       (equal? u v)))

(define (assp ? lst)
  (if (null? lst)
      #f
      (if (? (caar lst)) (car lst) (assp ? (cdr lst)))))

(define (walk u subst)
  (let ([maybe-mapped-val (and (var? u) (assp (λ (v) (var=? u v)) subst))])
    (if maybe-mapped-val (walk (cdr maybe-mapped-val) subst) u)))

(define (extend-subst var val subst)
  ;; FIXME: add occurs check here
  (cons (cons var val) subst))

;;; Let-subtytping idea: I can pass around an environment that keeps track of
;;; let-bound variables. I can dispatch on the reason; if it's a let-var, I can
;;; add it to the environment.
;;;
;;; When I go to call `walk' on u-base, I can check to see if the variable name
;;; is in that let-poly environment. If it is, I create a clone of the type that
;;; it is bound to, and I unify against that.
;;;
;;; I think this might work: the books suggest actually modifying the AST with a
;;; copy of the function (which the mention as being unsatisfying) or by somehow
;;; propogating the type through. I think this lookup system is like a lazy
;;; version of that.
;;;
;;; FOLLOW-UP: I think we might want to do something in the constraint
;;; generation because it is the part of this algorithm that is most aware of
;;; lexical scope. I think we might be able to emit a different kind of
;;; constraint that equates the bound var to a specially-tagged expression that
;;; the unifier can pick up and clone if needed.

;; unify-1 :: term × term × subst → subst, subst-hist
(define (unify-1 u-base v-base subst subst-hist reason err-handler)
  (let ([u (walk u-base subst)]
        [v (walk v-base subst)])
    ;; (printf "~a\t: Unfiying ~a {~a} with\n\t\t  ~a {~a}\n" reason u u-base v v-base)
    (cond
      ;; succeed: both u and v have already been unified to the same value
      [(and (var? u) (var? v) (var=? u v)) (values subst subst-hist)]

      ;; Check for let-bound function
      [(and (eq? reason 'let-var) (tFunc? v))
       ;; (pretty-print `(unify-1 ,u (tFunc ,(tFunc-dom v) ,(walk (tFunc-rng v) subst))))
       (values (extend-subst u (tFunc->Poly v subst) subst)
               (cons (merge-record u-base v-base reason) subst-hist))]

      ;; variable checking
      [(var? u)
       (values (extend-subst u v subst)
               (cons (merge-record u-base v-base reason) subst-hist))]
      [(var? v)
       (values (extend-subst v u subst)
               (cons (merge-record v-base u-base reason) subst-hist))]

      ;; Clone polymorphic functions prior to application
      [(tFuncPoly? u)
       ;; (pretty-print `(orig ,u clone ,(clone-poly (Poly->tFunc u) subst)))
       (unify-1 (clone-poly (Poly->tFunc u) subst) v subst subst-hist reason err-handler)]
      [(tFuncPoly? v)
       ;; (pretty-print `(orig ,v clone ,(clone-poly (Poly->tFunc v) subst)))
       (unify-1 u (clone-poly (Poly->tFunc v) subst) subst subst-hist reason err-handler)]

      ;; functions: unify domain, then range
      [(and (tFunc? u) (tFunc? v))
       (let-values ([(subst2 hist2) (unify-1 (tFunc-dom u) (tFunc-dom v)
                                             subst subst-hist 'func-domain err-handler)])
         (unify-1 (tFunc-rng u) (tFunc-rng v) subst2 hist2 'func-rng err-handler))]

      ;; last ditch: are these things the same type?
      [else (let-values ([(st1 _) (struct-info u)]
                         [(st2 _2) (struct-info v)])
              (if (eq? st1 st2)
                  (values subst subst-hist)
                  (err-handler u-base v-base subst subst-hist reason)))])))

;; unify :: (set|list eqConstraint) × subst → subst
(define (unify subst-lst [subst '()] [subst-hist '()] [err-handler default-error-handler])
  (cond
    ;; if all done, return the substitution
    [(empty? subst-lst) subst]
    ;; crack open the first equality constraint and unify the two
    ;; sides of the equation
    [else (let ([reason (constraint-reason (car subst-lst))]
                [lhs (eqConstraint-lhs (car subst-lst))]
                [rhs (eqConstraint-rhs (car subst-lst))])
            (let-values ([(new-subst new-hist) (unify-1 lhs rhs subst subst-hist reason err-handler)])
              (unify (cdr subst-lst) new-subst new-hist err-handler)))]))

;; Clones polymorphic function types:
;;   (tFunc (tVar x) (tVar x)) becomes
;;   (tFunc (tVar y) (tVar y))
;; where y is a fresh symbol
(define (clone-poly fn subst)
  (match fn
    [(tFunc d r)
     (let ([new-d (if (tVar? d) (tVar (gensym (tVar-name d))) d)])
       (tFunc new-d (clone-poly (walk/replace r d new-d subst) subst)))]
    [_ fn]))

(define (walk/replace ast-raw old-raw new-raw subst)
  ;; If we happen to be passed variables, try and find their types
  (let ([ast (walk ast-raw subst)]
        [old (walk old-raw subst)]
        [new (walk new-raw subst)])
    (if (equal? ast old)
        new
        (match ast
          [(tFunc d r) (tFunc (walk/replace d old new subst) (walk/replace r old new subst))]
          [_ ast]))))

(module+ test
  (test-case "clone-poly"
    (check-match (clone-poly (tFunc (tVar 'x) (tVar 'x)) '())
                 (tFunc (tVar (? symbol? new-x)) (tVar new-x)))
    (check-match (clone-poly (tFunc (tVar 'x) (tFunc (tNum) (tVar 'x))) '())
                 (tFunc (tVar new-x) (tFunc (tNum) (tVar new-x))))
    (check-match (clone-poly (tFunc (tVar 'x) (tFunc (tVar 'y) (tVar 'x))) '())
                 (tFunc (tVar new-x) (tFunc (tVar (not 'y)) (tVar new-x))))))

;; For testing:
;;  - good:
;;    (define s1 (unify (constraint-gen (tag-ast '((λ x (+ x 42)) 1)))))
;;  - bad:
;;    (define s2 (unify (constraint-gen (tag-ast '((λ x (+ x 42)) (λ y (+ y 1)))))))

;; Walk the AST and replace the tag given in `tag-ast' with the type found in
;; `subst'. This is mostly just to make it easier to read.
(define (patch-annotations tagged-ast subst)
  (let ([node (ast-node tagged-ast)])
    (match node
      [`(λ ,var ,body) (ast `(λ ,var ,(patch-annotations body subst))
                            (list (walk (tVar var) subst) '-> (walk (tExpr body) subst)))]
      [`(if ,c ,tc ,fc)
       (ast `(if ,(patch-annotations c subst)
                 ,(patch-annotations tc subst)
                 ,(patch-annotations fc subst))
            (walk (tExpr tagged-ast) subst))]
      [`(let (,var ,bind) ,body)
       (ast `(let (,var ,(patch-annotations bind subst))
               ,(patch-annotations body subst))
            (walk (tExpr tagged-ast) subst))]
      [`(,(and (or 'zero?) op) ,a)
       (ast `(,op ,(patch-annotations a subst)) (walk (tExpr tagged-ast) subst))]
      [`(,(and (or '+ '- '* '/) op) ,lhs ,rhs)
       (ast `(,op ,(patch-annotations lhs subst) ,(patch-annotations rhs subst))
            (walk (tExpr tagged-ast) subst))]
      [`(,f ,a) (ast (list (patch-annotations f subst)
                           (patch-annotations a subst))
                     (walk (tExpr tagged-ast) subst))]
      [(? symbol? s) (ast s (walk (tVar s) subst))]
      [_ (ast node (walk (tExpr tagged-ast) subst))])))

(define (infer-and-annotate ast)
  (let* ([tagged (tag-ast ast)]
         [constraints (constraint-gen tagged)]
         [subst (unify constraints)])
    (patch-annotations tagged subst)))

;;; Here is where we define some heruistics around producing good error
;;; messages.
;;;
;;; Note that in this this section you'll see references to "var" or "variable";
;;; perhaps this would have better been labeled as "expr" and "expression": in
;;; the context of this error handling section, a variable is anything that
;;; could reference a type or another variable in the substitution map.

;; Record of when two terms were merged
(struct merge-record (l r reason) #:transparent)

;; Global variables to aid in debugging
(define err-u '())    ; find the type of u with (walk err-u err-s)
(define err-v '())
(define err-s '())
(define err-h '())

(define (default-error-handler u-base v-base subst hist [reason #f])
  ;; Set the debugging variables
  (set! err-u u-base)
  (set! err-v v-base)
  (set! err-s subst)
  (set! err-h hist)
  (let ([u (walk u-base subst)] [v (walk v-base subst)])
    (printf (describe-error u-base u v-base v subst hist reason))
    (raise "type error")))

(define (string-join* . args)
  (string-join args))

;; Primary type info → error string function
(define (describe-error u-base u v-base v subst hist reason)
  ;; (pretty-print (list reason u-base u v-base v))
  ;; (printf "--------------------\n")
  ;; (pretty-print subst)
  ;; (printf "--------------------\n")
  ;; (pretty-print hist)
  ;; (printf "--------------------\n")
  (match reason
    ['func-domain (string-join* "Type error in arguments to function:"
                                (format "~a does not match ~a\n  ~a but\n  ~a"
                                        (pretty-type u subst) (pretty-type v subst)
                                        (source->string u-base subst hist)
                                        (source->string v-base subst hist)))]

    ;; With an operator argument, the first thing is always the errant argument,
    ;; and the second thing is always the expected type. This is because of the
    ;; constraing generation step on the operator aguments: we use this order.
    ['op-arg (string-join* "Error in arguments to operator:"
                           (format "Operator expected type ~a but got type ~a instead.\n"
                                   (pretty-type v subst) (pretty-type u subst))
                           (format "Argument ~a" (source->string u-base subst hist)))]

    ['cond-test (string-join* "Bad type in conditional:"
                              (format "`if' requires the conditional to be of type ~a but got type ~a instead.\n"
                                      (pretty-type (tBool) subst)
                                      (pretty-type u subst))
                              (source->string u-base subst hist))]

    ['cond-arm-match (string-join* "Both arms of a conditional must have the same type (or use if-union)\n"
                                   (format "true-case: ~a\n" (source->string u-base subst hist))
                                   (format "false-case: ~a\n" (source->string v-base subst hist)))]))

;;; Useful things to try testing:
;;;  - (infer-and-annotate '(if 1 2 3))
;;;  - (infer-and-annotate '(if (λ x (+ x 1)) 2 3))
;;;  - (infer-and-annotate '(if #t 2 #f))
;;;
;;;  - (infer-and-annotate '(let (x #t) (if (zero? x) 1 2)))
;;;  - (infer-and-annotate '(let (x (λ y y)) (if (zero? x) 1 2)))

;;; Some sub-par type reporting (difficult to fix, I think):
;;;  - (infer-and-annotate '(let (x (λ y (+ y 1))) (if (zero? 1) (x #t) 2)))

;; Find the reason why a var was given its type: searches the history of merges
;; to find where a particular expression/variable/thing was merged with
;; something else.
(define (find-reason var hist)
  (or                                   ; search left first, then right
   (findf (λ (m) (equal? var (merge-record-l m)))
          hist)
   (findf (λ (m) (equal? var (merge-record-r m)))
          hist)))

;; Given a variable, try and guess where it's type was derived from: was it the
;; argument to an operator? Or is it a literal value? Uses information from
;; `find-reason' to decided whether it shoudl try harder to find more
;; information. (e.g. in the case of a variable reference, where did *that*
;; variable get *its* type?)
(define (guess-type-source var subst hist)
  (match (find-reason var hist)
    [(merge-record l r 'var-ref) (guess-type-source (if (eq? var l) r l) subst hist)]
    [(merge-record l r src) (list src l r)]))

(define (source->string var subst hist)
  (match (guess-type-source var subst hist)
    [`(lit ,src ,type)
     (format "~a bound to or is a literal value of type ~a" (untag-expr src) (pretty-type type subst))]
    [`(let-var ,(tVar var) ,bind-expr)
     (format "variable ~a was bound to value of type ~a in let expression" var (pretty-type bind-expr subst))]
    [`(op-arg ,src ,type)
     (format "~a expected to be of type ~a by operator" (untag-expr src) (pretty-type type subst))]
    [`(op-res ,src ,type)
     (format "~a produces type ~a" (untag-expr src) (pretty-type type subst))]
    [`(fn-body ,src ,type)
     (format "~a is of type ~a" (untag-expr src) (pretty-type type subst))]
    [`(func-domain ,(tVar var) ,type)
     (format "parameter ~a in funciton has type ~a" var (pretty-type type subst))]
    [whatever "unmatched source: ~a" whatever]))

;; Pretty-print types: if variables are bound to a type, walk the substitution
;; until you come to a type or a polymorphic type variable.
(define (pretty-type var subst)
  (let ([res (walk var subst)])
    (match res
      [(tFunc (tVar v) rng) (format "(~a -> ~a)" (pretty-type (tVar v) subst) (pretty-type rng subst))]
      [(tNum) "ℕ"]
      [(tBool) "𝔹"]
      [(tVar v) (format "~a:~a" v (if (eq? res (walk res subst)) v (pretty-type res subst)))]
      [(tExpr _) (if (eq? res (walk res subst)) (format "~a" res) (pretty-type res subst))])))

;; Strip out all the (ast ... meta) crap and just let us see the ASTs roughly as
;; they were written.
(define (untag-expr expr)
  (match expr
    [(tExpr thing) (untag-expr thing)]
    [(ast thing _) (untag-expr thing)]
    [`(λ ,var ,body) `(λ ,var ,(untag-expr body))]
    [`(,(and (or '+ '- '* '/) op) ,lhs ,rhs)
     `(,op ,(untag-expr lhs) ,(untag-expr rhs))]
    [`(,(and (or 'zero?) op) ,a)
     `(,op ,(untag-expr a))]
    [`(,f ,a) (list (untag-expr f) (untag-expr a))]
    [(? symbol? s) s]
    [(? number? n) n]
    [(? boolean? b) b]))
